const getTask = (taskId) => {
  return tasks.find(task => task.id == taskId)
}

const findLastTasks = (projectId) => {
  return tasks.filter(task => task.projectId == projectId && !task.adjacentTo.length)
}

const updateStatusComplete = (taskId) => {
  // find task with the id

  // set status to be 'complete'
}

const findCurrentTasks = (projectId) => {
  // find 
  const result = []  
  const finalTasks = findLastTasks(projectId)
  const lastTasksIds = finalTasks.map(task => task.id)
  findLastCompletedRecursively(result, lastTasksIds)
  return result
}

const findLastCompletedRecursively = (array, tasksIds) => {
  if (!tasksIds || !tasksIds.length) return
  for(id of tasksIds) {
    const task = getTask(id)
    if (task.status == 'complete') {
      const isAlreadyInArray = array.find(e => e.id == task.id)
      !isAlreadyInArray && array.push(task)
      return
    } else if (task.status == 'open') {
      const adherentTasks = tasks.filter(task => {
        return task.adjacentTo.includes(id)
      })
      const adherentTasksIds = adherentTasks.map(task => task.id)
      findLastCompletedRecursively(array, adherentTasksIds)
    }
  }
}

// const taskToComplete = []

// const findLastOpenRecursively = (tasksIds) => {
//   if (!tasksIds.length) return
//   for(id of tasksIds) {
//     const task = getTask(id)
//     if (task.status == 'complete') {
//       console.log('last completed', task)
//     } else if (task.status == 'open') {
//       const adherentTasks = tasks.filter(task => {
//         return task.adjacentTo.includes(id)
//       })
//       const adherentTasksIds = adherentTasks.map(task => task.id)
//       findLastOpenRecursively(adherentTasksIds)
//     }
//   }
// }


const isComplete = (projectId) => {
// Get all the tasks

// Use the DAG to traverse

// return if the tasks are all complete

}
  
const tasks = [
  {
    id: '1',
    projectId: '1',
    type: 'get-property-address',
    status: 'complete',
    adjacentTo: ['2'],
  },
  {
    id: '2',
    projectId: '1',
    type: 'get-property-title-pdf',
    status: 'complete',
    adjacentTo: ['4'],
  },
  {
    id: '3',
    projectId: '1',
    type: 'get-sellers-full-name',
    status: 'open',
    adjacentTo: ['4'],
  },
  {
    id: '4',
    projectId: '1',
    type: 'sellers-name-matches-property-title-pdf',
    status: 'open',
    adjacentTo: [],
  },
  // {
  //   id: '5',
  //   projectId: '2',
  //   type: 'get-property-address',
  //   status: 'complete',
  //   adjacentTo: ['6'],
  // },
  // {
  //   id: '6',
  //   projectId: '2',
  //   type: 'get-property-title-pdf',
  //   status: 'complete',
  //   adjacentTo: ['8'],
  // },
  // {
  //   id: '7',
  //   projectId: '2',
  //   type: 'get-sellers-full-name',
  //   status: 'open',
  //   adjacentTo: ['8'],
  // },
  // {
  //   id: '8',
  //   projectId: '2',
  //   type: 'sellers-name-matches-property-title-pdf',
  //   status: 'open',
  //   adjacentTo: [],
  // }
]

// testing

// const lastTasks = findLastTasks('1')
// // console.log(lastTasks)

// // const currentTasks = findCurrentTasks('1')
// const lastTasksIds = lastTasks.map(task => task.id)
// console.log(lastTasksIds)
// findLastOpenRecursively(lastTasksIds)
// console.log(taskToComplete)

const result = findCurrentTasks('1')
console.log('last completed tasks', result)